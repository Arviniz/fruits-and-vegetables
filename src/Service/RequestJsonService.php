<?php
namespace App\Service;

use JetBrains\PhpStorm\ArrayShape;

class RequestJsonService {

    #[ArrayShape(['message' => "string", 'fruits' => "array", 'vegetables' => "array"])]
    public function processJson($json): array
    {
        $jsonData = json_decode($json, true);

        $fruitService = ProductServiceFactory::createFruitService();
        $vegetableService = ProductServiceFactory::createVegetableService();

        foreach ($jsonData as $item) {
            if ($item['type'] === 'fruit') {
                $fruitService->add($item['name'], $item['quantity'], $item['unit']);
            } elseif ($item['type'] === 'vegetable') {
                $vegetableService->add($item['name'], $item['quantity'], $item['unit']);
            }
        }

        // Prepare response content
        return [
            'message' => 'Data processed successfully.',
            'fruits' => $fruitService->list(),
            'vegetables' => $vegetableService->list(),
        ];
    }

}