<?php

namespace App\Service;

class BaseProductService
{
    protected array $collection;
    protected int $lastId;

    const UNIT_GRAMS = 'g';
    const UNIT_KILOGRAMS = 'kg';

    public function __construct($collection, $lastId)
    {
        $this->lastId = $lastId;
        $this->collection = $collection;
    }


    public function list(): array
    {
        return $this->collection;
    }

    public function add($name, $quantity, $unit, $id = null)
    {
        $product = ['name' => $name, 'quantity' => $quantity, 'unit' => $unit, "id" => $this->getId($id)];
        $this->collection[] = $product;
        return $product;
    }

    public function remove($id)
    {
        $this->collection = [...array_filter(
            $this->collection,
            fn($fruit) => $fruit['id'] !== $id
        )];
    }

    private function getId($id)
    {
        if ($id && !count(array_filter($this->collection, fn($fruit) => $fruit['id'] === $id))) {
            return $this->lastId;
        }
        return ++$this->lastId;
    }

    public function search(string $search): array
    {
        return [...array_filter(
            $this->collection,
            fn($product) => preg_match("/$search/", $product["name"])
        )];
    }

    public function changeUnit(string $unit = self::UNIT_GRAMS): array
    {
        $conversionFactors = [
            self::UNIT_GRAMS => 1,
            self::UNIT_KILOGRAMS => 1000,
        ];

        return array_map(
            fn($product) => [
                ...$product,
                "unit" => $unit,
                "quantity" => $this->convertQuantityToUnit($product, $unit, $conversionFactors),
            ],
            $this->collection
        );
    }

    private function convertQuantityToUnit(array $product, string $unit, array $conversionFactors): float
    {
        if (!isset($conversionFactors[$product["unit"]]) || !isset($conversionFactors[$unit])) {
            throw new \InvalidArgumentException("Invalid unit provided.");
        }

        $factorFrom = $conversionFactors[$product["unit"]];
        $factorTo = $conversionFactors[$unit];

        return $product["quantity"] * ($factorFrom / $factorTo);
    }
}
