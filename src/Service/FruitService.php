<?php

namespace App\Service;


use JetBrains\PhpStorm\Pure;

class FruitService extends BaseProductService
{

    #[Pure]
    public function __construct()
    {
        parent::__construct([], 0);
    }
}