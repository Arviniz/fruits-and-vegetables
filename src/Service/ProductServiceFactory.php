<?php
namespace App\Service;

class ProductServiceFactory
{
    public static function createFruitService()
    {
        return new FruitService();
    }

    public static function createVegetableService()
    {
        return new VegetableService();
    }
}