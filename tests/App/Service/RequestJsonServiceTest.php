<?php

namespace App\Tests\App\Service;

use App\Service\ProductServiceFactory;
use PHPUnit\Framework\TestCase;
use App\Service\RequestJsonService;


class RequestJsonServiceTest extends TestCase
{
    public function testProcessJson(): void
    {
        $request = file_get_contents('request.json');
        $requestJsonService = new RequestJsonService();
        $result = $requestJsonService->processJson($request);

        // Assertions
        $this->assertArrayHasKey('message', $result);
        $this->assertEquals('Data processed successfully.', $result['message']);
        $this->assertArrayHasKey('fruits', $result);
        $this->assertArrayHasKey('vegetables', $result);

        // Calculate the total count of fruits and vegetables
        $totalFruitsAndVegetablesCount = count($result['fruits']) + count($result['vegetables']);

        // Calculate the count of items in the input JSON
        $inputJsonCount = count(json_decode($request, true));

        // Assertions
        $this->assertEquals($inputJsonCount, $totalFruitsAndVegetablesCount);
    }
}
