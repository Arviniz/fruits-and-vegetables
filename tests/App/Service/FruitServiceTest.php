<?php

namespace App\Tests\App\Service;

use App\Service\FruitService;
use PHPUnit\Framework\TestCase;

class FruitServiceTest extends TestCase
{
    public function testAdd()
    {
        $fruitService = new FruitService();

        $fruitService->add('Apple', 10, 'kg');

        $fruits = $fruitService->list();

        $this->assertCount(1, $fruits);
        $this->assertEquals('Apple', $fruits[0]['name']);
        $this->assertEquals(10, $fruits[0]['quantity']);
        $this->assertEquals('kg', $fruits[0]['unit']);
        $this->assertEquals(1, $fruits[0]['id']);
    }

    public function testRemove()
    {
        $fruitService = new FruitService();

        $fruit = $fruitService->add('Apple', 10, 'kg');
        $fruitService->add('Banana', 5, 'kg');

        $fruitService->remove($fruit["id"]);

        $fruits = $fruitService->list();

        $this->assertCount(1, $fruits);
        $this->assertEquals('Banana', $fruits[0]['name']);
    }

    public function testSearch()
    {
        $fruitService = new FruitService();

        $fruitService->add('Apple', 10, 'kg');
        $fruitService->add('Banana', 5, 'kg');

        $searchResults = $fruitService->search('Banana');

        $this->assertCount(1, $searchResults);
        $this->assertEquals('Banana', $searchResults[0]['name']);
    }

    public function testChangeUnit()
    {
        $fruitService = new FruitService();

        $fruitService->add('Apple', 1000, 'g');

        $fruits=$fruitService->changeUnit(FruitService::UNIT_KILOGRAMS);

        $this->assertCount(1, $fruits);
        $this->assertEquals('Apple', $fruits[0]['name']);
        $this->assertEquals(1, $fruits[0]['quantity']);
        $this->assertEquals(FruitService::UNIT_KILOGRAMS, $fruits[0]['unit']);
    }
}
